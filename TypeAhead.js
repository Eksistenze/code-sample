import React from "react";
import "./typeAhead.css";

export default class TypeAhead extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      predict: [],
      text: "",
    };
  }

  onTyping = (e) => {
    const { items } = this.props;
    let predict = [];
    const val = e.target.value;
    if (val.length > 0) {
      const regex = new RegExp(`^${val}`, `i`);
      predict = items.sort().filter((v) => regex.test(v));
    }

    this.setState(() => ({
      predict,
    }));
    this.props.setName(val);
  };

  predictSelected = (val) => {
    this.setState(() => ({
      predict: [],
    }));
    this.props.setName(val);
    if (this.props.setSearch) {
      this.props.setSearch(val);
    }
  };

  renderPredict = () => {
    const { predict } = this.state;
    if (predict.length === 0) {
      return null;
    }
    return (
      <ul className="">
        {[...new Set(predict)].map((pred) => (
          <li
            key={pred}
            className="dropdown-item"
            onClick={(e) => this.predictSelected(pred)}
          >
            {pred}
          </li>
        ))}
      </ul>
    );
  };

  render() {
    const { text } = this.state;
    return (
      <div className="form-floating mb-3 type-ahead">
        <input
          onChange={this.onTyping}
          placeholder={this.props.placeholder}
          value={this.props.name}
          type="text"
        />
        <div className="type-ahead-div">{this.renderPredict()}</div>
      </div>
    );
  }
}
